#include <iostream>
#include <fstream>
#include <cctype>

#include <string>

#include "Vector.hpp"
#include "Map.hpp"

#include "Tests.hpp"

// Wanted to use my own implementation of string but ran into issues
// so decided to use std::string to get a working solution in time
using String = std::string;

const static char StartUpperCase = 'A';
const static char EndUpperCase = 'Z';

const static char StartLowerCase = 'a';
const static char EndLowerCase = 'z';

bool IsUpperCaseCharacter(const char c)
{
	return c >= StartUpperCase && c <= EndUpperCase;
}

bool IsLowerCaseCharacter(const char c)
{
	return c >= StartLowerCase && c <= EndLowerCase;
}

bool IsAlphabeticCharacter(const char c)
{
	if(IsUpperCaseCharacter(c)
		|| IsLowerCaseCharacter(c))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool IsSpace(const char c)
{
	return c == ' ';
}

char ConvertToLowerCase(const char c)
{
	return c + 32;
}

void SelectionSort(Vector<Map::MapEntry>& vec)
{
	Map::MapEntry temp;
	size_t minValueIndex = 0u;

	for (size_t i = 0; i < vec.GetSize() - 1; i++)
	{
		minValueIndex = i;

		for (auto j = i + 1; j < vec.GetSize(); j++)
		{
			if (vec[j].value > vec[minValueIndex].value)
			{
				minValueIndex = j;
			}
		}

		temp = vec[i];
		vec[i] = vec[minValueIndex];
		vec[minValueIndex] = temp;
	}
}

int main(int argc, char* argv[])
{
	tests::RunTests();

	if(argc != 2)
	{
		std::cerr << "Incorrect number of arguments" << std::endl;
		exit(1);
	}

	const char* fileName = argv[1];

	std::ifstream file(fileName);

	if(!file.is_open())
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		return -1;
	}

	std::string line;

	Map map;

	while(std::getline(file, line))
	{
		std::string word;

		for(const auto currentChar : line)
		{
			if(IsAlphabeticCharacter(currentChar))
			{
				if(IsUpperCaseCharacter(currentChar))
				{
					word += ConvertToLowerCase(currentChar);
				}
				else
				{
					word += currentChar;
				}
			}
			else if(!word.empty())
			{
				map.Insert(word);
				word.clear();
			}
		}

		if(!word.empty())
		{
			map.Insert(word);
		}
	}

	auto& mostFrequentWords(map.GetData());
	SelectionSort(mostFrequentWords);

	for(auto i = 0u; i < 20u; ++i)
	{
		std::cout << mostFrequentWords[i].value << "\t" << mostFrequentWords[i].key << std::endl;
	}

	return 0;
}
