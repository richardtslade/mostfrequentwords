#ifndef MAP_HPP
#define MAP_HPP

#include <inttypes.h>
#include <cmath>
#include <string>

#include "Vector.hpp"

// I started by implementing a hash map but this had issues with the hash function
// so then I wanted to implement a binary tree but ran out of time so I settled on the simplest option.
// This isn't really a map but a glorified vector
// With more time I would have made used a binary tree to store the data in the map
class Map
{
public:
	struct MapEntry
	{
		std::string key;
		uint32_t value;
	};

	Map() = default;

	using Iterator = MapEntry*;

	// I wanted to use the same format for this as with std::map but owing to
	// time restrictions I decided to go with this solution
	// this method will inccur over head due to the need for linear search each
	// time a new entry is added this could have be avoided by using a binary tree
	// to store the entries
	void Insert(std::string& word)
	{
		bool exists = false;

		for(auto& e : m_map)
		{
			if(e.key == word)
			{
				++e.value;
				exists = true;
				break;
			}
		}

		if(!exists)
		{
			m_map.PushBack(MapEntry{word, 1u});
		}
	}

	size_t GetSize() const
	{
		return m_map.GetSize();
	}

	Iterator begin()
	{
		return m_map.begin();
	}

	Iterator end()
	{
		return m_map.end();
	}

	Vector<MapEntry>& GetData()
	{
		return m_map;
	}

private:

	Vector<MapEntry> m_map;
};

#endif // MAP_HPP
