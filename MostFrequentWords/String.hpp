#ifndef STRING_HPP
#define STRING_HPP

#include <cstring>

#include "Vector.hpp"

// I sunk a bit of time into this class but ran into issues
// so decided to use std::string instead in order to get a working solution in time
class String
{
public:
	using Iterator = char*;
	using CIterator = const char* const;

	String() = default;

	String(const size_t size, const char* string)
		: m_string(size)
	{
		for(auto i = 0u; i < m_string.GetSize(); ++i)
		{
			m_string[i] = string[i];
		}
	}

	const char* GetString()
	{
		char* ret = new char[m_string.GetSize()];

		auto index = 0u;

		for(auto i = 0u; i < m_string.GetSize(); ++i)
		{
			ret[index++] = m_string[i];
		}

		return ret;
	}

	size_t GetSize() const
	{
		return m_string.GetSize();
	}

	bool IsEmpty() const
	{
		return m_string.IsEmpty();
	}

	char& operator [] (const size_t index)
	{
		return m_string[index];
	}

	Iterator begin()
	{
		return m_string.begin();
	}

	Iterator end()
	{
		return m_string.end();
	}

	void operator += (const char c)
	{
		m_string.PushBack(c);
	}

	bool operator == (String& s)
	{
		return std::strcmp(s.GetString(), this->GetString()) == 0u;
	}

private:
	Vector<char> m_string;
};

#endif // STRING_HPP
