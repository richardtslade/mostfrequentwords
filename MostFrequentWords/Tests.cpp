
#include <cassert>

#include "Tests.hpp"
#include "Vector.hpp"
#include "String.hpp"

namespace tests
{
// Wanted to add more tests here but ran out of time
bool RunVectorTests()
{
	size_t size = 10u;

	// Test construction
	{
		const Vector<int> v(size);
		assert(v.GetSize() == size);
	}

	// Test resevation
	{
		size = 0u;
		Vector<int> v(size);

		assert(v.IsEmpty());

		const size_t capacity = 10u;
		v.Reserve(capacity);

		assert(v.GetCapacity() == capacity);
	}

	// Test resizing
	{
		size = 0u;
		Vector<int> v(size);

		const size_t newSize = 10u;
		v.Resize(newSize);

		assert(v.GetSize() == newSize);
	}

	// Test data modifiers
	{
		size = 3u;
		Vector<int> v(size);

		v[0] = 1u;
		v[1] = 2u;
		v[2] = 3u;

		auto expectedValue = 1;

		for(const auto& i : v)
		{
			assert(i == expectedValue);
			++expectedValue;
		}

		const int newValue = 4;
		v.PushBack(newValue);

		const int newExpectedSize = 4u;

		assert(v.GetSize() == newExpectedSize);
		assert(v[newExpectedSize - 1] == newValue);
	}

	return true;
}

// Didn't have time to implement these, with more time they would have helped me
// diagnose and fix the issues I ran into with my implementation
bool RunStringTests()
{
	// Test construction
	{
		const char* testString = "Test";
		String s(5u, testString);
	}

	return true;
}


// Again ran out of time to implement these tests these would be necessary if
// my implementation of map had used a binary tree implementation
bool RunMapTests()
{
	return true;
}

bool RunTests()
{
	bool result = true;

	result &= RunVectorTests();
	result &= RunStringTests();
	result &= RunMapTests();

	return result;
}
} // namespace tests
