#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <cstddef>

// This is a basic implementation but serves the purposes I needed it for
// if necessary and with more time I would have completed the interface
// to have parity with std::vector
template<typename T>
class Vector
{
public:
	using Iterator = T*;
	using CIterator = const T* const;

	Vector() = default;

	Vector(const size_t size)
		: m_capacity(size)
		, m_size(size)
	{
		m_data = new T[size];
	}

	Vector(const size_t size, const T defaultValue)
		: m_capacity(size)
		, m_size(size)
	{
		m_data = new T[size];

		for(auto i = 0u; i < size; ++i)
		{
			m_data[i] = defaultValue;
		}
	}

	~Vector()
	{
		if(m_data)
		{
			delete[] m_data;
		}
	}

	void Reserve(const size_t newCapacity)
	{
		T* newData = new T[newCapacity];

		for(auto i = 0u; i < m_size; ++i)
		{
			newData[i] = m_data[i];
		}

		if(m_data)
		{
			delete[] m_data;
		}

		m_capacity = newCapacity;
		m_data = newData;
	}

	void Resize(const size_t newSize)
	{
		Reserve(newSize);
		m_size = newSize;
	}

	void PushBack(const T& newElement)
	{
		if(m_size >= m_capacity)
		{
			Reserve(m_capacity + 5);
		}

		m_data[m_size++] = newElement;
	}

	const T* GetData() const
	{
		return m_data;
	}

	size_t GetSize() const
	{
		return m_size;
	}

	size_t GetCapacity() const
	{
		return m_capacity;
	}

	bool IsEmpty() const
	{
		return m_size == 0u;
	}

	Iterator begin()
	{
		return m_data;
	}

	Iterator end()
	{
		return m_data + m_size;
	}

	T& operator [] (const size_t index)
	{
		return m_data[index];
	}

	bool operator == (Vector<T>& v)
	{
		if(v.GetSize() != this->GetSize())
		{
			return false;
		}

		auto index = 0u;

		for(auto& e : v)
		{
			if(e != this->m_data[index++])
			{
				return false;
			}
		}

		return true;
	}

private:
	size_t m_capacity = 0u;
	size_t m_size = 0u;
	T* m_data = nullptr;
};

#endif // VECTOR_HPP
